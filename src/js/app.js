import $ from 'dom7';
import Framework7 from 'framework7/bundle';

// Import F7 Styles
import 'framework7/css/bundle';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.scss';


// Import Routes
import routes from './routes.js';
// Import Store
import store from './store.js';

// Import main app component
import App from '../app.f7';
import Dom7 from 'dom7';


var app = new Framework7({
  name: 'AppLatihan', // App name
  view: {
    stackPages: true
  },
  theme: 'auto', // Automatic theme detection
  el: '#app', // App root element
  component: App, // App main component

  // App store
  store: store,
  // App routes
  routes: routes,
});

var mainView = app.views.create('.view-main');
var $$ = Dom7;

function cek(){
  var status = localStorage.getItem("status");
  if (status == "login"){
    app.views.main.router.navigate("/");
  }else{
    app.views.main.router.navigate("/login/");
  }
}