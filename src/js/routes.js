
import HomePage from '../pages/home.f7';
import LoginPage from '../pages/login.f7'

var routes = [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/login/',
    component: LoginPage,
  }
];

export default routes;